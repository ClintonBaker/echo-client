import { useContext, useEffect } from "react";
import { UserContext } from "../utils/UserContext";
import { Outlet, redirect, useNavigate } from "react-router-dom";

export const loader = async () => {
  try {
    const res = await fetch("http://localhost:5000/verify-token", {
      method: "GET",
      credentials: "include",
    });
    const data = await res.json();
    return null;
  } catch {
    console.log("redirect to login...");
    return redirect("/login");
  }
};

const Root = () => {
  const { user, fetchUserData } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    fetchUserData();
  }, []);

  useEffect(() => {
    !user && navigate("/login");
  }, [user]);

  return (
    <div className="App">
      <Outlet />
    </div>
  );
};

export default Root;
