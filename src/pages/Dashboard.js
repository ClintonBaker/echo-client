import { useContext } from "react";
import { UserContext } from "../utils/UserContext";

export const Dashboard = () => {
  const { user, logOut } = useContext(UserContext);

  return (
    <div className="dashboard">
      Hello! <button onClick={logOut}>Log Out</button>
      {user && user.username}
    </div>
  );
};
