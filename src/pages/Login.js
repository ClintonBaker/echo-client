import { useContext, useEffect, useState } from "react";
import { UserContext } from "../utils/UserContext";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";
import Logo from "../assets/logo-b.png";

export const Login = () => {
  const { user, logIn, register } = useContext(UserContext);
  const navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(true);
  const [errorMsg, setErrorMsg] = useState("");

  const username = "Bob";
  const email = "bob@bob.com";
  const password = "bob1";

  useEffect(() => {
    user && navigate("/");
  }, [user]);

  const onSubmit = async (e) => {
    e.preventDefault();
    if (errorMsg) setErrorMsg("");

    const body = {
      username: e.currentTarget.username.value,
      password: e.currentTarget.password.value,
    };

    if (!isLogin) {
      body.email = e.currentTarget.email.value;
      if (body.password !== e.currentTarget.rpassword.value) {
        setErrorMsg(`The passwords don't match`);
        return;
      }
      register(body);
    } else {
      logIn(body.username, body.password);
    }
  };

  return (
    <Container>
      <Header src={Logo} width="127px" height="50px" />
      <Form onSubmit={onSubmit}>
        {!isLogin && (
          <Input type="text" name="email" placeholder="Email" required />
        )}

        <Input type="text" name="username" placeholder="Username" required />

        <Input
          type="password"
          name="password"
          placeholder="Password"
          required
        />

        {!isLogin && (
          <Input
            type="password"
            name="rpassword"
            placeholder="Verify Password"
            required
          />
        )}

        {errorMsg && <Error>*{errorMsg}</Error>}

        <Submit>
          <Button type="submit">{isLogin ? "Login" : "Signup"}</Button>
          <Link
            onClick={(e) => {
              e.preventDefault();
              setIsLogin(!isLogin);
            }}
          >
            {isLogin ? "I don't have an account" : "I already have an account"}
          </Link>
        </Submit>
      </Form>
    </Container>
  );
};

const Header = styled.img`
  margin-bottom: 15px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 200px;
`;

const Form = styled.form`
  display: flex;
  flex-flow: column;
  align-items: center;
  width: 250px;
`;

const Input = styled.input`
  :focus {
    outline: none;
  }
  padding: 10px;
  margin-bottom: 7px;
  border: none;
  border-radius: 3px;
  width: 100%;
`;

const Submit = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  width: 100%;
  margin-top: 10px;
`;

const Button = styled.button`
  height: 35px;
  border: none;
  border-radius: 3px;
  width: 100%;
  background: var(--secondary);
  color: var(--primary);
`;

const Link = styled.a`
  margin-top: 4px;
  color: var(--accent);
`;

const Error = styled.p`
  color: var(--error);
  margin-bottom: 4px;
  font-size: 12px;
`;
