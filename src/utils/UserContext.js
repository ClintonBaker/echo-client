import { createContext, useReducer } from "react";

export const UserContext = createContext();

const userReducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      return { ...state, user: action.payload, isAuthenticated: true };
    case "REGISTER":
      return { ...state, user: action.payload, isAuthenticated: true };
    case "LOGOUT":
      return { ...state, user: null, isAuthenticated: false };
    default:
      throw new Error(`Unknown action: ${action.type}`);
  }
};

export const UserProvider = ({ children }) => {
  const [state, dispatch] = useReducer(userReducer, {
    user: null,
    isAuthenticated: false,
  });

  const logIn = async (username, password) => {
    const res = await fetch("http://localhost:5000/login", {
      method: "POST",
      body: JSON.stringify({ username, password }),
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });

    const { user } = await res.json();

    if (user) {
      dispatch({ type: "LOGIN", payload: user });
    }
  };

  const register = async (newUser) => {
    const res = await fetch("http://localhost:5000/register", {
      method: "POST",
      body: JSON.stringify(newUser),
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });

    const { user } = await res.json();

    if (user) {
      dispatch({ type: "REGISTER", payload: user });
    }
  };

  const logOut = async () => {
    const res = await fetch("http://localhost:5000/logout", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      credentials: "include",
    });
    dispatch({ type: "LOGOUT" });
  };

  const fetchUserData = async () => {
    try {
      const res = await fetch("http://localhost:5000/verify-token", {
        method: "GET",
        credentials: "include",
      });
      const { user } = await res.json();

      dispatch({ type: "LOGIN", payload: user });
    } catch (err) {}
  };

  return (
    <UserContext.Provider
      value={{ user: state.user, logIn, register, logOut, fetchUserData }}
    >
      {children}
    </UserContext.Provider>
  );
};
